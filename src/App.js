import React, { Component } from 'react';

import './App.css';
import Square from './Square/Square';

class App extends Component {
    state = {
        squares: [],
        tries: 0,
        end: false
    };

    generateSquares = () => {
        let squares = [];

        for (let i = 0; i < 36; i++) {
             let status = {
                 isOpen: false,
                 hasRing: false
             };
            squares.push(status);
        }
        let randomIndex = Math.floor(Math.random() * squares.length);
        console.log(randomIndex);
        squares[randomIndex].hasRing = true;

        this.setState({squares, tries:0});

    };

    openSquare = (i) => {
        let squares = [...this.state.squares];
        squares[i].isOpen = true;

        this.setState(prevState => ({squares: squares, tries: ++prevState.tries}));

    };


    render() {

       return (
        <div className="App">
            {this.state.squares.map((square, i) => {
               return <Square
                   key={i}
                   click = {() => this.openSquare(i)}
                   isOpen = {square.isOpen}
                   hasRing = {square.hasRing}
               />
            }

            )}
            <p>Tries: {this.state.tries}</p>
        <button onClick={this.generateSquares} > Reset </button>
        </div>
    )
}




}

export default App;
