import React from 'react';
import './Square.css';

const Square = props => {
    return (
        <div className = {props.isOpen ? 'Square open' : 'Square'}  onClick={props.click} >
            {props.hasRing && props.isOpen ? '0' : null}
        </div>
    )
};

export default Square;